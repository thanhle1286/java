package Example_Java;

public class NhanVien extends ConNguoi implements HoatDongTieuCuc {
	private String maNhanVien;
	private String phongBan;
	private double mucluong;

	public NhanVien() {

	}

	public NhanVien(String maNhanVien, String phongBan, double mucluong) {

		this.maNhanVien = maNhanVien;
		this.phongBan = phongBan;
		this.mucluong = mucluong;
	}

	public void gioithieu() {
		System.out.println("Tôi tên là " + this.getTen() + ", tôi là nhân viên phòng " + this.phongBan
				+ " với mã số là " + this.maNhanVien);
	}

	// public NhanVien(String maNhanVien, String phongBan, double mucluong) {
	// System.out.println(" Tôi là tên nhân viên , tôi là nhân viên phòng phòng
	// ban với mã số là mã nhân viên");
	// }

	@Override
	public void choiCobac() {
		System.out.println(this.getTen() + " chơi co bac ");

	}

	@Override
	public void uongRuou() {
		System.out.println(this.getTen() + " uống rượu ");

	}

	@Override
	public void hutThuoc() {
		System.out.println(this.getTen() + " hút thuốc ");

	}

	@Override
	void sudungMayTinh() {
		System.out.println(this.getTen() + " sử dụng máy  ");

	}

	public String getMaNhanVien() {
		return maNhanVien;
	}

	public void setMaNhanVien(String maNhanVien) {
		this.maNhanVien = maNhanVien;
	}

	public String getPhongBan() {
		return phongBan;
	}

	public void setPhongBan(String phongBan) {
		this.phongBan = phongBan;
	}

	public double getMucluong() {
		return mucluong;
	}

	public void setMucluong(double mucluong) {
		this.mucluong = mucluong;
	}

}
