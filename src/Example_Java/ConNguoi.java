package Example_Java;

public abstract class ConNguoi {
	private String ten;
	private String ngaysinh;
	private int gioitinh;
	private String diachi;

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getNgaysinh() {
		return ngaysinh;
	}

	public void setNgaysinh(String ngaysinh) {
		this.ngaysinh = ngaysinh;
	}

	public int getGioitinh() {
		return gioitinh;
	}

	public void setGioitinh(int gioitinh) {
		this.gioitinh = gioitinh;
	}

	public String getDiachi() {
		return diachi;
	}

	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}

	public void an() {
		System.out.println(this.ten + " dang an");
	}

	public void ngu() {
		System.out.println(this.ten + " dang ngu");
	}

	public void chay() {
		System.out.println(this.ten + " dang chay");
	}

	public void choi() {
		System.out.println(this.ten + " choi");
	}

	public void gioithieu() {
		System.out.println(this.ten + " dang gioi thieu");
	}

	abstract void sudungMayTinh();
}
