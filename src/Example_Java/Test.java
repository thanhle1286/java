package Example_Java;

public class Test {
	public static void main(String args[]) {
		NhanVien nhanvien1 = new NhanVien();
		nhanvien1.setTen("Lê Minh Thành");
		nhanvien1.setMaNhanVien("NV000010");
		nhanvien1.setPhongBan("ACCOUNTTING");
		// call method nhanvien1
		nhanvien1.an();
		nhanvien1.chay();
		nhanvien1.choi();
		nhanvien1.choiCobac();
		nhanvien1.getTen();
		nhanvien1.gioithieu();
		nhanvien1.hutThuoc();
		nhanvien1.ngu();
		nhanvien1.uongRuou();
		
		// call method nhanvien2
		NhanVien nhanvien2 = new NhanVien("Lê Minh Thành", "NV000010", 1.1);
		nhanvien2.an();
		nhanvien2.chay();
		nhanvien2.choi();
		nhanvien2.choiCobac();
		nhanvien2.getTen();
		nhanvien2.gioithieu();
		nhanvien2.hutThuoc();
		nhanvien2.ngu();
		nhanvien2.uongRuou();

	    SinhVien sinhvien1 = new SinhVien();
	    sinhvien1.setTen("THANH");
	    sinhvien1.setNgaysinh("01/12/1986");
	    sinhvien1.setMaSinhVien("SV0000011");
	    sinhvien1.setLop("CLA");
	    sinhvien1.setGioitinh(1);
	    sinhvien1.setEmail("thanhlm@nal.vn");
	    sinhvien1.setDiachi("Chien Thang, Ha Dong, Ha Noi");
	    // call method sinhvien1
	    sinhvien1.an();
	    sinhvien1.chay();
	    sinhvien1.choi();
	    sinhvien1.docsach();
	    sinhvien1.gioithieu();
	    sinhvien1.hoctap();
	    sinhvien1.uongCapheSang();
	    // call method sinhvien2
	    SinhVien sinhvien2 = new SinhVien("SV0000011", "CLA", "thanhlm@nal.vn");
	    sinhvien2.setTen("THANH");
	    sinhvien2.an();
	    sinhvien2.chay();
	    sinhvien2.choi();
	    sinhvien2.docsach();
	    sinhvien2.gioithieu();
	    sinhvien2.hoctap();
	    sinhvien2.uongCapheSang();
	    

	}
}
