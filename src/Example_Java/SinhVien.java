package Example_Java;

public class SinhVien extends ConNguoi implements HoatDongTichCuc {

	private String maSinhVien;
	private String lop;
	private String email;

	public SinhVien() {
		// TODO Auto-generated constructor stub
	}

	public void gioithieu() {
		System.out.println("Tôi tên là " + this.getTen() + ", tôi là sinh viên với mã số là " + this.maSinhVien);
	}

	public SinhVien(String maSinhVien, String lop, String email) {
		super();
		this.maSinhVien = maSinhVien;
		this.lop = lop;
		this.email = email;

	}

	public String getMaSinhVien() {
		return maSinhVien;
	}

	public void setMaSinhVien(String maSinhVien) {
		this.maSinhVien = maSinhVien;
	}

	public String getLop() {
		return lop;
	}

	public void setLop(String lop) {
		this.lop = lop;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public void docsach() {
		System.out.println(this.getMaSinhVien() + " đọc sách");

	}

	@Override
	public void hoctap() {
		System.out.println(this.getMaSinhVien() + " học ");

	}

	@Override
	public void luyentapThethao() {
		System.out.println(this.getMaSinhVien() + " luyện tập thể ");

	}

	@Override
	public void nghenhac() {
		System.out.println(this.getMaSinhVien() + " nghe ");

	}

	@Override
	public void uongCapheSang() {
		System.out.println(this.getMaSinhVien() + " uống cà phê sáng");

	}

	@Override
	void sudungMayTinh() {
		System.out.println(this.getMaSinhVien() + " sử dụng máy ");

	}

}
